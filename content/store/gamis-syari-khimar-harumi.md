+++
badge = "Limited Edition"
berat = 600
color = "Mint"
date = "2018-04-05T11:12:33+07:00"
description = "Gamis Harumi hadir sebagai koleksi terbaru Oribelle Hijab Style dengan desain simpel dan elegan. Dirancang dengan memadukan basic dress bermotif dan khimar instan satu layer."
image_1 = "/images/harumi-1.png"
image_2 = "/images/harumi-2.png"
image_3 = "/images/harumi-3.png"
image_4 = "/images/harumi-4.png"
image_5 = "/images/harumi-2.png"
link_product = "https://belanjaqu.co.id/apinco"
merek = "ORIBELLE"
price = "Rp. 350.000"
product = ["gamis", "hijab"]
size = "Panjang Depan: 85 cm Panjang Belakang: 125 cm"
slug = ""
stock = true
title = "Gamis Syari + Khimar Harumi"

+++
Gamis Harumi hadir sebagai koleksi terbaru Oribelle Hijab Style dengan desain simpel dan elegan. Dirancang dengan memadukan basic dress bermotif dan khimar instan satu layer.  
  
Size  
All Size (Fit to L) : Lingkar Dada 100 cm Panjang 140 cm  
Dipasang karet di belakang gamis  
  
Material  
Cotton Zhegna high quality dengan spesifikasi: sejenis Ballotelly kualitas premium, lebih lembut, lebih jatuh, tidak menerawang, adem dan nyaman dipakai.  
  
Keunggulan Harumi:  
- Basic dress dengan cutting yang nyaman khas Oribelle.  
- Model kerah shanghai klasik dan manis dengan bukaan resleting depan agar nursing friendly (Busui).  
- Dipasang karet di belakang gamis untuk mengatur ukuran.  
- Cocok dipadukan dengan khimar instan satu layer.  
- Aksen rampel cantik dan list yang senada dengan khimar di pergelangan tangan agar wudhu friendly.  
- Dipasang saku kiri dan kanan di samping gamis.  
- Cocok dipakai di segala suasana.  
  
KHIMAR  
Khimar instan pet antem satu lapis dengan model semi penguin dengan hiasan list polos yang senada di bagian bawah khimar.  
  
Size KHIMAR  
Panjang Depan: 85 cm  
Panjang Belakang: 125 cm  
  
Material  
Moza Crepe dengan spesifikasi: sejenis crepe bermotif bunga, tidak menerawang, dan nyaman dipakai.  