+++
badge = "baru"
berat = "1200 gr"
color = ""
date = "2018-03-30T13:52:37+07:00"
description = "Gamis Fiona hadir dengan motif abstract warna-warni didesain chic and simple."
image_1 = "/images/fiona-1.png"
image_2 = "/images/fiona-2.png"
image_3 = "/images/fiona-3.png"
image_4 = "/images/fiona-4.png"
image_5 = "/images/fiona-5.png"
link_product = "https://www.bandros.co.id/produk/gamis-syari-dress-syari-setelan-gamis-khimar-fiona"
price = "Rp. 325.000"
product = ["gamis", "dress"]
size = "80 cm x 120 cm"
merek = "Oribelle"
slug = ""
stock = true
title = "Gamis Syari + Khimar Fiona"

+++
Gamis Fiona hadir dengan motif abstract warna-warni didesain chic and simple. Basic dress fiona terbuat dari bahan kaos dengan motif unik yang dipadukan dengan khimar instan satu layer pet antem dan aksen renda di bawahnya. Keserasian antara balutan dan warna membuat gamis syari ini cocok dipakai sahabat muslimah saat bersilaturahmi dengan keluarga besar khususnya di hari raya.  
  
Size Gamis:  
All Size (Fit to L): Lingkar Dada 100 cm Panjang 140 cm  
\(Dipasang karet di belakang gamis, stretch dan bisa melar)  
  
Size Khimar:  
Panjang Depan: 80 cm  
Panjang Belakang 120 cm  
  
Material Gamis:  
Bagian Motif Motif: Interlocked Twisted Yarn (ITY) Yona premium dengan spesifikasi ITY Yona: adem, jatuh, menyerap keringat, tidak mudah kusut, tebal, stretch dan nyaman dipakai.  
Bagian Polos: IRIS Crepe dengan spesifikasi: bahan kaos crepe lembut, tebal, stercth, adem, jatuh, dan nyaman dipakai.  
  
Material Khimar:  
IRIS Crepe dengan spesifikasi: bahan kaos crepe lembut, tebal, stercth, adem, jatuh, dan nyaman dipakai.  
  
Keunggulan Fiona:  
- Desain simpel dengan cutting khas Oribelle yang nyaman.  
- Cocok dikreasikan dan dikombinasikan dengan khimar instan satu layer pet antem dan renda di ujungnya.  
- Model kerah Shanghai dengan bukaan resleting depan agar nursing friendly (busui).  
- Aksen kerut di bagian pergelangan tangan agar wudhu friendly.  
- Dipasang karet di belakang gamis agar ukuran gamis bisa fleksibel.  
- Cocok dipakai saat kumpul dengan keluarga besar.  
- Khimar satu lapis dengan dengan pet khas Oribelle yang bisa membuat tirus pipi pemakainya, selain itu dibuat renda di bagian bawah khimar.  
  
  
Note:  
Foto model asli (real picture). Objek utama tidak direkayasa, kalaupun ada sedikit perbedaan antara foto dengan aslinya maksimal 5% – 10% karena pengaruh cahaya sekitar.  