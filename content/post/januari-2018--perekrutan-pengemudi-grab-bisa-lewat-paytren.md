+++
categories = ["news"]
date = "2018-03-30T13:00:02+07:00"
description = "Grab, perusahaan yang melayani jasa transportasi online, mengumumkan kerja sama strategis dengan PayTren, perusahaan yang bergerak di bidang teknologi finansial."
featured = []
thumbnail = "/images/grab.png"
slug = ""
tags = ["grab", "paytren"]
title = "Januari 2018, Perekrutan Pengemudi Grab Bisa Lewat Paytren"

+++
[Grab](https://www.tempo.co/tag/grab), perusahaan yang melayani jasa transportasi online, mengumumkan kerja sama strategis dengan PayTren, perusahaan yang bergerak di bidang teknologi finansial (fintech) milik Ustad Yusuf Mansur, hari ini, Rabu, 13 Desember 2017. Kerja sama ini memungkinkan keduanya untuk mengintegrasikan masing-masing mitra dan mitra pengemudi.

Segera setelah menandatangani perjanjian kerja sama ini, para mitra PayTren akan diberikan pelatihan untuk mendaftarkan mitra pengemudi Grab yang baru. Perekrutan mitra pengemudi Grab dapat dilakukan mulai pertengahan Januari 2018 dan akan menjadi fitur tambahan dalam aplikasi PayTren.

 Kemitraan dengan PayTren dinilai akan membawa Grab lebih maju dalam memberdayakan 3 juta pengusaha mikro pada awal 2018.

Ongki Kurniawan, Managing Director GrabPay Indonesia, mengatakan melalui kemitraan dengan PayTren, Grab menjalankan komitmen yang sama untuk membawa peluang ekonomi digital kepada kelas ekonomi menengah, baik di daerah perkotaan maupun perdesaan di Tanah Air.

**Baca**: [Garuda Indonesia-Grab Berkolaborasi, Ini yang Ditawarkan](https://bisnis.tempo.co/read/1041637/garuda-indonesia-grab-berkolaborasi-ini-yang-ditawarkan)

"Kini, kami membantu masyarakat untuk bergabung sebagai mitra pengemudi di lebih dari 100 kota dan agen di 500 kota. Dengan menambahkan kekuatan dan kehadiran PayTren di daerah-daerah pedesaan Indonesia, kami yakin dapat mempercepat ekspansi Grab di seluruh Indonesia," tuturnya.

Sementara itu, Yusuf Mansur, Founder & Owner Paytren, mengatakan bermitra dengan Grab merupakan langkah PayTren dalam menunjukkan eksistensinya sebagai salah satu pelaku usaha dunia Financial Technology dan pemberdayaan UMKM yang memang sudah menjadi core business PayTren dalam pemberdayaan umat.

"Kami memiliki visi yang sama akan masa depan Indonesia. Jangkauan kemitraan yang dimiliki oleh PayTren sudah nationwide, kami ingin memberdayakan kalangan menengah lndonesia dan membantu mereka memperoleh penghasilan tambahan dengan memanfaatkan ekonomi digital," katanya.

Dia menambahkan sebagai langkah awal, mitra-mitra PayTren dapat memperoleh penghasilan dari pendaftaran para pengemudi ke platform [Grab](https://www.tempo.co/tag/grab). "Kami sangat antusias untuk menjajaki cara-cara alternatif agar manfaat ekonomi digital dapat dirasakan dalam kehidupan sehari-hari masyarakat indonesia," katanya.

sumber: [https://bisnis.tempo.co/read/1041911/januari-2018-perekrutan-pengemudi-grab-bisa-lewat-paytren](https://bisnis.tempo.co/read/1041911/januari-2018-perekrutan-pengemudi-grab-bisa-lewat-paytren "https://bisnis.tempo.co/read/1041911/januari-2018-perekrutan-pengemudi-grab-bisa-lewat-paytren")