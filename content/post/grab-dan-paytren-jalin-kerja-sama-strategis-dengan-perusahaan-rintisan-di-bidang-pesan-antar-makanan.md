+++
categories = ["news"]
date = "2018-03-30T10:34:07+07:00"
description = "Grab, platform penyedia layanan transportasi on-demand dan pembayaran mobile terdepan di Asia Tenggara, hari ini mengumumkan kerja sama strategis dengan Madhang.id"
featured = []
thumbnail = "/images/IMG_5050-512x288.jpg"
slug = "paytren-madhang-grab"
tags = ["paytren"]
title = "Grab dan PayTren Jalin Kerja Sama Strategis dengan Perusahaan Rintisan"

+++
Grab, platform penyedia layanan transportasi _on-demand_ dan pembayaran _mobile_ terdepan di Asia Tenggara, hari ini mengumumkan kerja sama strategis dengan Madhang.id, salah satu perusahaan rintisan di bidang pesan antar makanan rumahan asal Semarang. 

Penandatanganan perjanjian kerja sama antara kedua belah pihak memungkinkan Madhang.id dan Grab untuk mengintegrasikan kekuatan teknologi Grab yang terdapat dalam aplikasinya dengan aplikasi pesan antar Madhang.id. 

Kerja sama tersebut akan meliputi kerja sama dalam infrastruktur digital, infrastruktur pengantaran makanan, promosi program rewards dan layanan Grab for Business.

Pada saat yang sama, PayTren, salah satu aplikasi pembayaran dan transaksi mobile terkemuka di Indonesia, juga melakukan penandatanganan perjanjian kerja sama dengan Madhang.id untuk mengintegrasikan fasilitas pembayaran secara online yang dimiliki PayTren dengan aplikasi Madhang.id. 

Melalui integrasi ini para pelanggan Madhang.id akan dapat melakukan pembayaran makanan yang dipesan melalui Madhang.id dengan menggunakan PayTren. 

Penandatanganan kerja sama diantara ketiga pihak ini merupakan tindak lanjut dari komitmen bersama Grab dan PayTren yang dicetuskan pada Desember 2017 lalu untuk mendorong pembangunan ekonomi yang berbasis partisipasi masyarakat luas sebagai pelaku usaha dan percepatan persebaran lapangan usaha digital di Tanah Air sebagai dampak dari kemajuan ekonomi digital.

[![](https://news.treni.co.id/wp-content/uploads/2018/01/IMG_5032-512x341.jpg =512x341)](https://news.treni.co.id/wp-content/uploads/2018/01/IMG_5032.jpg)

**Kaesang Pangarep, Lead Marketing Madhang.id**mengatakan, “Melalui kemitraan dengan Grab, Madhang.id akan mendapatkan banyak manfaat dalam hal pembuatan dan pengaktifan penanda lokasi digital untuk setiap tenant aplikasi Madhang.id, pengaktifan _slot digital_ yang bisa dimanfaatkan tenant aplikasi Madhang.id untuk memungkinkan pelanggan Madhang.id menikmati layanan pesan antar makanan melalui GrabExpress, serta beberapa manfaat lainnya dalam hal promosi dan rewards serta Grab For Business bagi karyawan kami.

 Disaat yang bersamaan kami juga berharap semakin banyak masyarakat khususnya ibu-ibu rumah tangga yang telah menjadi tenant kami untuk mendapatkan akses dan manfaat dari berbagai macam layanan Grab yang telah hadir di lebih dari 100 kota di Indonesia.”

“Kami sangat gembira dapat mendukung usaha Madhang.id dalam hal pembayaran _mobile_ yang merupakan kekuatan utama dari aplikasi PayTren. Sebagai salah satu pelaku usaha dunia _Financial Technology_, pemberdayaan UMKM seperti Madhang.id memang sudah menjadi _core business_ PayTren dalam pemberdayaan umat, kami memiliki visi yang sama akan masa depan Indonesia. 

Dengan jangkauan kemitraan yang dimiliki oleh PayTren sudah _nationwide_, kami ingin memberdayakan kalangan menengah Indonesia dan membantu mereka memperoleh penghasilan tambahan dengan memanfaatkan ekonomi digital.” papar **Ustadz Yusuf Mansur, Founder & Owner PayTren**.

**Ridzki Kramadibrata, Managing Director Grab Indonesia** mengatakan, “Kemitraan dengan Madhang.id sejalan dengan komitmen Grab dalam _masterplan 2020_ “Grab For Indonesia” untuk mendukung perusahaan rintisan/_startup_ yang berfokus pada industri  layanan _mobile_ dan teknologi dengan penekanan layanan di kota-kota kecil dan komunitas yang belum merasakan manfaat dari ekonomi digital. 

Kami juga melihat bahwa kerja sama ini juga mendukung usaha kami untuk membawa peluang ekonomi digital kepada kelas ekonomi menengah baik di daerah perkotaan maupun pedesaan di Tanah Air seperti yang telah kami lakukan bagi 2,3 juta mitra pengemudi di Asia Tenggara. ”

Penandatangan kerja sama antara ketiga pihak ini dilaksanakan di sela-sela acara “Setahun Grab Jawa Tengah” di Kantor Gubernuran, Semarang sebagai perayaan satu tahun kehadiran Grab di Jawa Tengah. Acara ini merupakan bentuk apresiasi atas kerja keras dan komitmen para mitra pengemudi di Jawa Tengah yang telah turut berpartisipasi dalam mendorong pencapaian luar biasa Grab di Tanah Air.

[![](https://news.treni.co.id/wp-content/uploads/2018/01/IMG_5011-512x341.jpg =512x341)](https://news.treni.co.id/wp-content/uploads/2018/01/IMG_5011.jpg)

Sebagai bagian dari komitmennya untuk meningkatkan kualitas hidup para mitra pengemudi dan keluarganya dan pengakuan atas kontribusi mereka kepada perusahaan, Grab mengaungerahkan penghargaan kepada puluhan mitra pengemudi GrabCar dan GrabBike terbaik di wilayah Semarang.

“Kerja sama Para mitra pengemudi kamilah yang telah membentuk Grab menjadi penyedia layanan transportasi terpercaya di Indonesia. Kami berterima kasih atas dukungan dan kerja keras para mitra pengemudi di Jawa Tengah yang telah berusaha memberikan pelayanan terbaik kepada pelanggan dan menjunjung tinggi standar keselamatan Grab.” lanjut Ridzki.

Saat ini, Grab menyediakan layanan GrabCar, GrabBike, GrabTaxi dan GrabFood untuk menjawab kebutuhan masyarakat Jawa Tengah akan layanan transportasi yang aman, nyaman, dan juga terjangkau. Layanan Grab dapat dinikmati di seluruh kabupaten di Jawa Tegah yang mencakup Pati Raya, Semarang Raya, Pekalongan Raya dan Banyumas Raya.

  sumber: [https://news.treni.co.id/paytren-madhang-grab/](https://news.treni.co.id/paytren-madhang-grab/ "https://news.treni.co.id/paytren-madhang-grab/")